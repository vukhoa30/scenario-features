Feature: Login
  Scenario: Login successfully
    Given I'm at the login page
    When I input the right username/password
    Then I'm redirected to dashboard
  Scenario: Login unsuccessfully
    Given I'm at the login page
    When I input the wrong username/password
    Then a error message "wrong credentials" shows up 