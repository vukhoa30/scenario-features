[PTC-7637] - [Checkout] [Shipping Info] Display Shipping Options Based on the Country Selection
UGPW-4458 - [QE/UI] Design layout view of CA Site checkout
UGPW-4456 - [QE/UI] Design layout view of US Site Checkout

Narrative:
As a customer, I want to view the shipping methods that
I have available to me for the shipping address that I have entered.

Meta:
@Author Velta Fisher
@acceptance PTC-7637 UGPPTCUI-430
@country US CA MX INTL

Scenario: verifying user is able to select shipping method
Meta:
@c1

Given 1 regular product is added to Cart via services for a current UI session
When user opens checkout page:
{wtf=man}
|1|2|3|
|one|two|three|
When user fills/edits shipping address for different countries: com/aeo/testdata/common/shippingAddress/<shippingAddress>
line 11
line 2
And user clicks shipping options block
Before: wth mann
Then verifying info message is displayed for a moment
And verifying info message is not displayed
When user selects shipping method <shipMethod>
Then shipping method <shipMethod> is selected

Examples:
|Meta:                        |shippingAddress         |shipMethod |
|@country US                  |address_US_CA.table     |Standard   |
|@country US                  |address_US_CA.table     |Two Day    |
|@country US                  |address_US_CA.table     |Overnight  |
|@country US @skip UGPW-7009|address_US_CA.table     |SHOP RUNNER|
|@country CA                  |address_CA_MB.table     |Standard   |
|@country INTL                |address_INTL_India.table|Express    |
|@country INTL                |address_INTL_India.table|Standard   |


Scenario: verifying selected shipping method still persist after cart item is removed from checkout page
Meta:
@history PTC-7889
@c2
@not_stable because of step - processing spinner.


Given 1 regular product is added to Cart via services for a current UI session
And user adds product GD_regPrd1 with 1 quantity to cart from services
When user opens checkout page
And user fills/edits shipping address for different countries: com/aeo/testdata/common/shippingAddress/<shippingAddress>
And user clicks shipping options block
Then verifying info message is displayed for a moment
And verifying info message is not displayed
When user selects shipping method <shipMethod>
Then shipping method <shipMethod> is selected
When user removes product GD_regPrd1 from checkout page
Then processing spinner is displayed for a moment
And processing spinner is not displayed
And shipping method <shipMethod> is selected



Examples:
|Meta:                        |shippingAddress         |shipMethod |
|@country US                  |address_US_CA.table     |Standard   |
|@country US                  |address_US_CA.table     |Two Day    |
|@country US                  |address_US_CA.table     |Overnight  |
|@country US @skip UGPW-7009|address_US_CA.table     |SHOP RUNNER|
|@country CA                  |address_CA_MB.table     |Standard   |
|@country INTL                |address_INTL_India.table|Express    |
|@country INTL                |address_INTL_India.table|Standard   |

